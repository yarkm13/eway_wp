<?php

class EasyWay_Widget extends WP_Widget {
    /**
     * Unique identifier for your widget.
     *
     *
     * The variable name is used as the text domain when internationalizing strings
     * of text. Its value should match the Text Domain file header in the main
     * widget file.
     *
     * @since    1.0.0
     *
     * @var      string
     */
    protected $widget_slug = 'easyway';

    /**
     * Return the widget slug.
     *
     * @since    1.0.0
     *
     * @return    Plugin slug variable.
     */
    public function get_widget_slug() {
        return $this->widget_slug;
    }
    
    /**
     * Sets up a new EasyWay widget instance.
     *
     * @since 1.0.0
     */
    public function __construct() {
      $widget_ops = array(
        'classname' => 'easyway_city_widget',
        'description' => __( 'EasyWay city routes widget.', 'easyway' ),
        'customize_selective_refresh' => true,
      );
      parent::__construct( 'easyway', _x( 'EasyWay', 'EasyWay widget' ), $widget_ops );
    }
    
    /**
     * Outputs the content for the current EasyWay widget instance.
     *
     * @since 1.0.0
     *
     * @param array $args     Display arguments including 'before_title', 'after_title',
     *                        'before_widget', and 'after_widget'.
     * @param array $instance Settings for the current EasyWay widget instance.
     */
    public function widget( $args, $instance ) {
      // Check if there is a cached output
      $cache = wp_cache_get( $this->get_widget_slug(), 'widget' );
      if ( !is_array( $cache ) )
        $cache = array();
      if ( ! isset ( $args['widget_id'] ) )
        $args['widget_id'] = $this->id;
      if ( isset ( $cache[ $args['widget_id'] ] ) )
        return print $cache[ $args['widget_id'] ];

      $widget_string = $args['before_widget'];
      ob_start();
      $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Public transport:', 'easyway' );
      $city = ! empty( $instance['city'] ) ? $instance['city'] : get_option( 'easyway_city' );
    
      /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
      $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
    
      echo $args['before_widget'];
      if ( $title ) {
        echo $args['before_title'] . $title . $args['after_title'];
      }
      $url = 'https://api.easyway.info/?login=gps_widget&password='.get_option( 'easyway_api_password' ).'&function=cities.GetRoutesList&city='.get_option( 'easyway_city' );
      $server_data = wp_remote_get( esc_url_raw( $url ) );
      $server_data = json_decode($server_data['body'], true);
      $routes_key = __( 'Routes:', 'easyway' );
      $online_key = __( 'Online:', 'easyway' );
      $city_data = [$routes_key => [], $online_key => []];

      foreach ($server_data['routesList']['route'] as $route)
      {
          $type = $route['transport'];
          switch ($route['transport'])
          {
            case 'boat':
              $type = '⛴';
              break;
            case 'trol':
              $type = '🚎';
              break;
            case 'bus':
              $type = '🚌';
              break;
            default:
            $type = '❓';
              break;
          }
          if (!isset($city_data[$routes_key][$type]))
          {
            $city_data[$routes_key][$type] = 0;
          }
          if (!isset($city_data[$online_key][$type]))
          {
            $city_data[$online_key][$type] = 0;
          }
          $city_data[$routes_key][$type]++;
          $city_data[$online_key][$type] += $route['gps_count'];
      }
      ?>
      <div>
        <img src="https://static.easyway.info/new.eway/images/header_logo.png?1" class="aligncenter-" />
        <br />
              <?php foreach ($city_data as $line => $data){
                  $str_data = [];
                  foreach ($data as $type => $count) {
                    $str_data[] = $type.$count;
                  }
                  echo $line.' '.implode('/', $str_data).'<br/>';
              }
              ?>
        <a href="https://www.eway.in.ua/ua/cities/<?=$instance['city']?>" class="aligncenter" target="_blank" ><?php _e( 'See on the map', 'easyway' ) ?></a>
      </div>
      <?php
      $widget_string .= ob_get_clean();    
      $widget_string .= $args['after_widget'];
      $cache[ $args['widget_id'] ] = $widget_string;
      wp_cache_set( $this->get_widget_slug(), $cache, 'widget', 60 );
      echo $widget_string;
    }

    
    /**
     * Outputs the settings form for the EasyWay widget.
     *
     * @since 1.0.0
     *
     * @param array $instance Current settings.
     */
    public function form( $instance ) {
      $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'city' => get_option( 'easyway_city' )) );
      $title = $instance['title'];
      $city = $instance['city'];
      ?>
      <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
      <p><label for="<?php echo $this->get_field_id('city'); ?>"><?php _e('City:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('city'); ?>" name="<?php echo $this->get_field_name('city'); ?>" type="text" value="<?php echo esc_attr($city); ?>" /></label></p>
      <?php
    }
  
    /**
     * Handles updating settings for the current EasyWay widget instance.
     *
     * @since 1.0.0
     *
     * @param array $new_instance New settings for this instance as input by the user via
     *                            WP_Widget::form().
     * @param array $old_instance Old settings for this instance.
     * @return array Updated settings.
     */
    public function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $new_instance = wp_parse_args((array) $new_instance, array( 'title' => '', 'city' => ''));
      $instance['title'] = sanitize_text_field( $new_instance['title'] );
      $instance['city'] = sanitize_text_field( $new_instance['city'] );
      return $instance;
    }    
      
}