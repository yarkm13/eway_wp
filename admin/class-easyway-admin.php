<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    EasyWay
 * @subpackage EasyWay/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    EasyWay
 * @subpackage EasyWay/admin
 * @author     Your Name <email@example.com>
 */
class EasyWay_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The options name to be used in this plugin
	 *
	 * @since  	1.0.0
	 * @access 	private
	 * @var  	string 		$option_name 	Option name of this plugin
	 */
	private $option_name = 'easyway';	
	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in EasyWay_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The EasyWay_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/easyway-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in EasyWay_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The EasyWay_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/easyway-admin.js', array( 'jquery' ), $this->version, false );

	}

	/*
	 * Add an options page under the Settings submenu
	 *
	 * @since  1.0.0
	 */
	public function add_options_page() {
		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'EasyWay Settings', 'easyway' ),
			__( 'EasyWay', 'easyway' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_options_page' )
		);
	}

	/**
	 * Render the options page for plugin
	 *
	 * @since  1.0.0
	 */
	public function display_options_page() {
		include_once 'partials/easyway-admin-display.php';
	}
	
	public function register_setting() {
		register_setting( $this->plugin_name, $this->option_name . '_api_password' );
		register_setting( $this->plugin_name, $this->option_name . '_city' );

		// Add a General section
		add_settings_section(
			$this->option_name . '_general',
			__( 'General', 'easyway' ),
			array( $this, $this->option_name . '_general_cb' ),
			$this->plugin_name
		);		

		add_settings_field(
			$this->option_name . '_api_password',
			__( 'API password', 'easyway' ),
			array( $this, $this->option_name . '_api_password_cb' ),
			$this->plugin_name,
			$this->option_name . '_general',
			array( 'label_for' => $this->option_name . '_api_password' )
		);

		add_settings_field(
			$this->option_name . '_city',
			__( 'City', 'easyway' ),
			array( $this, $this->option_name . '_city_cb' ),
			$this->plugin_name,
			$this->option_name . '_general',
			array( 'label_for' => $this->option_name . '_city' )
		);
	}

	/**
	 * Render the text for the general section
	 *
	 * @since  1.0.0
	 */
	public function easyway_general_cb() {
		echo '<p>' . __( 'Please change the settings accordingly.', 'easyway' ) . '</p>';
	}
	
	
	/**
	 * Render the city input for this plugin
	 *
	 * @since  1.0.0
	 */
	public function easyway_api_password_cb() {
		$pass = get_option( $this->option_name . '_api_password' );
		echo '<input type="text" name="' . $this->option_name . '_api_password' . '" id="' . $this->option_name . '_api_password' . '" value="' . $pass . '"> ';
	}

	/**
	 * Render the city input for this plugin
	 *
	 * @since  1.0.0
	 */
	public function easyway_city_cb() {
		$city = get_option( $this->option_name . '_city' );
		echo '<input type="text" name="' . $this->option_name . '_city' . '" id="' . $this->option_name . '_city' . '" value="' . $city. '"> ';
	}

}
