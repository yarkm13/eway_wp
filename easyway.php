<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           EasyWay
 *
 * @wordpress-plugin
 * Plugin Name:       EasyWay widget plugin
 * Plugin URI:        https://gitlab.com/yarkm13/eway_wp
 * Description:       EasyWay widget displaying routes and transport online for selected city.
 * Version:           1.0.0
 * Author:            Yaroslav Kontsvyi
 * Author URI:        http://fb.me/yarkm13
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       easyway
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'EASYWAY_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-easyway-activator.php
 */
function activate_easyway() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-easyway-activator.php';
	EasyWay_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-easyway-deactivator.php
 */
function deactivate_easyway() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-easyway-deactivator.php';
	EasyWay_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_easyway' );
register_deactivation_hook( __FILE__, 'deactivate_easyway' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-easyway.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_easyway() {

	$plugin = new EasyWay();
	$plugin->run();

}
run_easyway();
